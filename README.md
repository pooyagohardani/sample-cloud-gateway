# Sample Cloud Gateway

This is a simple project to show some basic and core features of Spring Cloud Gateway.

+ ##### You need to install [buildpacks](https://buildpacks.io/docs/app-journey/) to be able to create docker images using the provided sh file.
