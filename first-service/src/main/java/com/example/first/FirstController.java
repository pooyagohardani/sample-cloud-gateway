package com.example.first;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/first")
public class FirstController {

    private static final Logger LOG = LoggerFactory.getLogger(FirstController.class);

    @GetMapping("get-message")
    public String getMessage() {
        return "This is a message from the FirstController";
    }

    @GetMapping("/secure/get-message")
    public String getSecuredMessage() {
        return "This is a message highly secured message from the FirstController";
    }

    @GetMapping("/secure/resource")
    public String resource(@AuthenticationPrincipal Jwt jwt,
                           @AuthenticationPrincipal OAuth2User oauth2User) {
        LOG.trace("***** JWT Headers: {}", jwt.getHeaders());
        LOG.trace("***** JWT Claims: {}", jwt.getClaims().toString());
        LOG.trace("***** JWT Token: {}", jwt.getTokenValue());
        LOG.trace("***** Name of the user: {}", oauth2User.getName());
        return String.format("Resource accessed by: %s (with subjectId: %s)",
                jwt.getClaims().get("user_name"),
                jwt.getSubject());
    }
}
