package com.example.second;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecondService {

	public static void main(String[] args) {
		SpringApplication.run(SecondService.class, args);
	}

}
