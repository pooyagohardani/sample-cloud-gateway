#!/bin/bash

echo "Performing a clean Maven build"
./mvnw clean package -DskipTests=true

#echo "Building the UAA"
#cd uaa || exit
#docker build --tag sample-cloud-uaa .
#cd ..

echo "Building the Registry"
cd registry || exit
docker build --tag sample-cloud-registry .
cd ..

echo "Building the First Service"
cd first-service || exit
docker build --tag sample-cloud-first-service .
cd ..

echo "Building the Second Service"
cd second-service || exit
docker build --tag sample-cloud-second-service .
cd ..

echo "Building the Gateway"
cd gateway || exit
docker build --tag sample-cloud-gateway .
cd ..
